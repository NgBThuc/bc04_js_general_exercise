/* 
(*) Viết hàm chia bài cho 4 người chơi
*/

let players = [[], [], [], []];
let cards = [
  "4K",
  "KH",
  "5C",
  "KA",
  "QH",
  "KD",
  "2H",
  "10S",
  "AS",
  "7H",
  "9K",
  "10D",
];

document.querySelector(".js__ex8__btn").addEventListener("click", () => {
  let resultMarkup = "";
  cards.forEach((card, index) => {
    players[index % players.length].push(card);
  });
  players.forEach((player, index) => {
    resultMarkup += `<li>Player ${index + 1}: [${player.join(", ")}]</li>`;
  });
  document.querySelector(".js__ex8__result").innerHTML = resultMarkup;
});
