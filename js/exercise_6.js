/* 
(*) Tìm x nguyên dương lớn nhất, biết 1+2+3+...+x ≤100
*/

document.querySelector(".js__ex6__btn").addEventListener("click", function () {
  let sum = 0;
  let i = 0;
  do {
    i++;
    sum += i;
  } while (sum <= 100);

  document.querySelector(".js__ex6__result").textContent = i - 1;
});
