/* 
In một bảng số từ 1-100 thỏa mãn điều kiện: (gợi ý dùng 2 vòng for lồng nhau, để ý chỗ bước nhảy)
- Bảng số gồm 10 hàng và 10 cột
- Các giá trị trong hàng là liên tiếp nhau
- Các giá trị trong cột hơn kém nhau 10
*/
let tableBody = document.querySelector(".js__ex1__tableBody");
let isOpen = false;

function printTable() {
  for (let i = 1; i < 100; i = i + 10) {
    let tableRow = document.createElement("tr");
    tableBody.appendChild(tableRow);
    for (let j = i; j < i + 10; j++) {
      let currentTableRow =
        tableBody.querySelectorAll("tr")[Math.floor(i / 10)];
      let tableData = document.createElement("td");
      tableData.textContent = j;
      currentTableRow.appendChild(tableData);
    }
  }
}

function removeTable() {
  let tableRowList = tableBody.querySelectorAll("tr");
  tableRowList.forEach((tableRow) => {
    tableRow.remove();
  });
}

document.querySelector(".js__ex1__btn").addEventListener("click", function () {
  if (!isOpen) {
    printTable();
    isOpen = true;
  } else {
    removeTable();
    isOpen = false;
  }
});
