/* 
Viết function nhận vào tham số là một mảng số nguyên,tìm và in ra các số nguyên tố trong mảng
*/

let numberList = [];

function isPrime(number) {
  if (number < 2) {
    return false;
  }
  for (let i = 2; i <= Math.sqrt(number); i++) {
    if (number % i === 0) {
      return false;
    }
  }
  return true;
}

document
  .querySelector(".js__ex2__addBtn")
  .addEventListener("click", function () {
    let newNumber = document.querySelector(".js__ex2__addInput").value * 1;
    numberList.push(newNumber);
    document.querySelector(".js__ex2__numberList").textContent = numberList;
    document.querySelector(".js__ex2__addInput").value = "";
    document.querySelector(".js__ex2__addInput").focus();
  });

document
  .querySelector(".js__ex2__findPrime")
  .addEventListener("click", function () {
    let filteredNumberList = numberList.filter((number) => isPrime(number));
    document.querySelector(".js__ex2__result").textContent = filteredNumberList;
  });
