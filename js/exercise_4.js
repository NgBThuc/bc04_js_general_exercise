/* 
(*) Viết function nhận vào tham số n, tính số lượng ước số của n.
*/

document
  .querySelector(".js__ex4__addNumberBtn")
  .addEventListener("click", function () {
    let number = document.querySelector(".js__ex4__addNumberInput").value * 1;
    let result = [];
    for (let i = 1; i <= number; i++) {
      if (number % i === 0) {
        result.push(i);
      }
    }
    document.querySelector(".js__ex4__result").textContent = result.join(", ");
  });
