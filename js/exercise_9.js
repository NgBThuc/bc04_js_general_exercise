/* 
(*) Viết chương trình nhập vào m là tổng số chó và gà, n là tổng số chân, yêu cầu tìm số lượng chó và gà
*/

document.querySelector(".js__ex9__btn").addEventListener("click", () => {
  let totalAnimal = +document.querySelector(".js__ex9__totalAnimalInput").value;
  let totalLeg = +document.querySelector(".js__ex9__totalLegInput").value;
  let chickenAmount = 2 * totalAnimal - totalLeg / 2;
  let dogAmount = totalLeg / 2 - totalAnimal;
  document.querySelector(
    ".js__ex9__result"
  ).textContent = `Có ${chickenAmount} con gà và ${dogAmount} con chó`;
});
