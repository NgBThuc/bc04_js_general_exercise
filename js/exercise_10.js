/* 
Nhập vào số giờ và số phút => góc lệch giữa kim giờ và kim phút
*/

document.querySelector(".js__ex10__btn").addEventListener("click", () => {
  let hourInput = +document.querySelector(".js__ex10__hourInput").value;
  let hour = hourInput >= 12 ? hourInput - 12 : hourInput;
  let minute = +document.querySelector(".js__ex10__minuteInput").value;
  let angleHourPerMinute = 360 / (60 * 12);
  let angleMinutePerMinute = 360 / 60;
  let angleHour = hour * 60 * angleHourPerMinute + minute * angleHourPerMinute;
  let angleMinute = minute * angleMinutePerMinute;
  let result =
    Math.abs(angleHour - angleMinute) > 180
      ? 360 - Math.abs(angleHour - angleMinute)
      : Math.abs(angleHour - angleMinute);
  document.querySelector(".js__ex10__result").textContent = `${result} độ`;
});
