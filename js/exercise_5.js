/* 
(*) Viết chương trình tìm số đảo ngược của 1 số nguyên dương n nhập từ bàn phím.
*/

document
  .querySelector(".js__ex5__reverseBtn")
  .addEventListener("click", function () {
    let number = document.querySelector(".js__ex5__numberInput").value;
    let result = [];
    for (let i = number.length - 1; i >= 0; i--) {
      result.push(number[i]);
    }
    document.querySelector(".js__ex5__result").textContent = result.join("");
  });
