/* 
Viết function nhận vào tham số n, tính S=(2+3+4...+n)+2n
*/

document
  .querySelector(".js__ex3__calcS")
  .addEventListener("click", function () {
    let number = document.querySelector(".js__ex3__numberInput").value * 1;
    let result = 0;
    for (let i = 2; i <= number; i++) {
      result += i;
    }
    result = result + 2 * number;
    document.querySelector(".js__ex3__result").textContent = result;
  });
