/* 
(*) Viết function nhận vào số n, in ra bảng cửu chương tương ứng với số đó.
*/

document.querySelector(".js__ex7__btn").addEventListener("click", function () {
  let number = +document.querySelector(".js__ex7__input").value;
  let resultMarkup = "";
  for (let i = 0; i <= 10; i++) {
    resultMarkup += `<div>${number} x ${i} = ${number * i}</div>`;
  }
  document.querySelector(".js__ex7__result").innerHTML = resultMarkup;
});
